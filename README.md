FYI This project is a junior frontend position assignment.
> (Hope that README file will make your life easier when reading my code)

# Project map
    * Folders:
        - sass folder - development
        - css folder - production
        - assets - including the icons and background of the project
    * Files:
        - sass/_base.scss - base settings of the project
        - sass/_components.scss - repeated elements
        - sass/_layout.scss - layout of the page & styling
        - sass/_mixing.scss - media query functions
        - sass/_main.scss - concating all sass files
        - css/normalize.css - reset the default css of the browser -> READ MORE HERE https://github.com/necolas/normalize.css
        - css/style.comp.css - file that compile the sass to css from sass/_main.scss file
        - css/style.prefix.css - file with all the css rules of the page with prefixes included (this file is not served to the users)
        - css/style.css - the final version of the css that served to the user (minified version)


# CSS Breakpoints:
    - phones - all the devices that smaller than 600px
    - tablets (portrait mode) - 600 to 900 px
    - tablets (landscape mode) - 900 to 1200 px
    - desktops - 1200 px and larger
    


# Node packages that I used trough development:
    - node-sass - css preprocessor
    - live-server
    - autoprefixer - automatically add prefixes to css for browsers support
    - concat - concating files together
    - postcss - a tool for transforming styles with JS plugins. 
    - npm-run-all -  tool to run multiple npm-scripts in parallel
